Evan Stephen Thomas
===================

gitlab.com/ethom

Summary of Skills
-----------------
### Algorithm Design and Optimization
* Implementation and use of machine learning methods: 
  supervised and unsupervised learning, generative neural networks, 
  auto-encoders, convolutional neural networks, NeuroEvolution training, 
  random forests, and Gaussian processes.

* Researched methods for integrating machine learning into design and 
  optimization processes. Demonstrated that careful integration of ML in 
  design optimization can facilitate solutions to otherwise intractable 
  problems.

* Knowledgeable in discrete mathematics, computational methods, and Bayesian 
  and Frequentist statistics. These skills have been applied to model 
  classical and quantum physics, epidemiology, and financial and economic 
  predictions.

### Python Programming
* Used for Master's research, Engineering Capstone, and six university 
  courses including statistics, computational methods and machine learning. 
  Primary programming language, with more than 6500 hours experience. 

* Able to produce modular, and extensible code quickly. 
  Code is readable and interoperable for team projects.

### Automation and Shell Scripting
* Automates workflows with coreutils and regex, and original command-line 
  applications.

* Learned POSIX shell and Bash by managing a home-cloud, personal Linux 
  computers, and compiling Android ROMs.

* Migrated documents between servers using PowerShell. Accounted for file 
  type, destination, duplication and date modified.

### Professional Skills
* Creative problem solver. Excels in dynamic, innovative, 
  and team-oriented workplaces. 

* Compelling presenter. Able to simplify complex topics without compromising 
  scientific integrity.

* Proficient in technical report writing. Fluent in English and French 
  (DELF B2 bilingual). 

* Prolific documentation writer and code commenter.

Education
---------

### M.Sc. Physics 
**National Research Council of Canada and the University of Ottawa (2019 - Present)**

* Thesis: ``Machine Learning Surrogates for Inverse Design'', 
  Supervisor: Isaac Tamblyn

* Studying machine learning and computational methods to develop predictive 
  and generative models for automated design. 

* Trained surrogate ML models to replace a conventional physics simulation 
  necessary in a genetic design optimization.

### B.Eng. Engineering Physics and Management. Minor in Mathematics
**McMaster University (2019)**

* Interdisciplinary engineering focused on fundamental physics. 
  Studied simulation, applied maths, nuclear and biomed eng.
* Management: a specialized double major in Commerce. 
  Applied skills from technical courses for small business management 
  consulting, economic predictions and marketing analytics.

* Engineering Capstone: Contributed to group thesis project by developing 
  and testing original filtering algorithms in Python for a distance-tracking 
  measurement device. Algorithms included Kalman filtering and 
  Bessel-function spectral filtering. 

Past Employment
---------------
### Employment and Social Development Canada, Data Scientist
**Labour Program, Ottawa, Ontario (May 2021 - August 2022)**

* Redesigned and revised a fuzzy matching python library. This increased 
  program speed by \~10x. Applied this to detect, cluster and recommend 
  action on non-identical duplicated records.

* Aggregated Canadian labour data from disparate and flawed natural language 
  datasets using SQL.

* Designed and implemented python and SQL scripts for documents migration 
  from a legacy Oracle SQL database. Documents were successfully migrated to 
  a SharePoint instance with original PowerShell scripts.

### Canadian Nuclear Laboratories, Research Student
**Computational Material Science, Chalk River, Ontario (May-August 2017, 2018)**

* Implemented a novel physics simulation using a computationally efficient 
  Finite Element Method. 
* Developed a constrained 2D tessellation algorithm and so initiated a new 
  direction for research, resulting in a journal publication.

Interests
---------
### Improvisational Comedy (2015 - Present)
* McMaster Improv Team, Co-President. 2017 - 2019
* University of Ottawa's ``MI6 Improv'', Coach. 2020 - Present
